package com.burak.cafe.controller;

import com.burak.cafe.entity.User;
import com.burak.cafe.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserDAO userDAO;

    @RequestMapping("/form")
    public String showUserForm(Model model)
    {
        User user=new User();
        model.addAttribute("user",user);
        return "user-form";
    }

    @RequestMapping("/saveUser")
    public String saveUser(@ModelAttribute("user") User user)
    {
        userDAO.save(user);
        return "redirect:/";
    }

    @RequestMapping("/list")
    public String listUsers(Model model)
    {
        model.addAttribute("user",userDAO.findAll());
        return "user-list";
    }

    @RequestMapping("/delete")
    public String deleteUsers(@RequestParam("userID") int id)
    {
        deleteUsers(id);
        return "redirect:/";
    }

}
