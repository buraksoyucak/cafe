package com.burak.cafe.controller;

import com.burak.cafe.entity.Category;
import com.burak.cafe.dao.CategoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryDAO categoryDAO;

    @RequestMapping("/form")
    public String addCategory(Model model)
    {
        Category category=new Category();
        model.addAttribute("categoryy", category);

        return "category-form";
    }

    @RequestMapping("/saveCategory")
    public String saveCategory(@ModelAttribute("categoryy") Category category)
    {
        categoryDAO.save(category);
        return "redirect:/";
    }


    @RequestMapping("/list")
    public String showList(Model model)
    {
        model.addAttribute("category1",categoryDAO.findAll());
        return "category-list";
    }


}
