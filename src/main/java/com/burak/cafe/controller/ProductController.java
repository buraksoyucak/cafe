package com.burak.cafe.controller;

import com.burak.cafe.dao.CategoryDAO;
import com.burak.cafe.entity.Category;
import com.burak.cafe.entity.Product;
import com.burak.cafe.dao.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private CategoryDAO categoryDAO;


    @RequestMapping("/add")
    public String addProduct(Model model,Model model1)
    {
        Product product=new Product();
        List<Category> categories=categoryDAO.findAll();
        model.addAttribute("product",product);
        model1.addAttribute("categories",categories);
        return "product-form";
    }

    @PostMapping("/save")
    public String saveProduct(@ModelAttribute("product") Product product)
    {
        productDAO.save(product);
        return "redirect:/";
    }

    @RequestMapping("/list")
    public String showProduct(Model model)
    {
        model.addAttribute("urunler",productDAO.findAll());
        return "product-list";
    }
}
