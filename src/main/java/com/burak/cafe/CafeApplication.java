package com.burak.cafe;

import com.burak.cafe.controller.HomeController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.burak.cafe.repositories"})
@ComponentScan(basePackages = {"com.burak.cafe.dao.Imp"},basePackageClasses = HomeController.class)
@EntityScan(basePackages = {"com.burak.cafe.entity"})
public class CafeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CafeApplication.class, args);
	}
}
