package com.burak.cafe.dao;

import com.burak.cafe.entity.User;

import java.util.List;

public interface UserDAO {
    User save(User user);
    List<User> findAll();
    User findOne(long userid);
}
