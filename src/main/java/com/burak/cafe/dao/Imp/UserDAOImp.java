package com.burak.cafe.dao.Imp;

import com.burak.cafe.entity.User;
import com.burak.cafe.repositories.UserRepository;
import com.burak.cafe.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDAOImp implements UserDAO {

    @Autowired
    UserRepository userRepository;


    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User findOne(long userid) {
        return null;
    }
}
