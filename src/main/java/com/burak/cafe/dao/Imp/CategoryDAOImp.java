package com.burak.cafe.dao.Imp;

import com.burak.cafe.entity.Category;
import com.burak.cafe.repositories.CategoryRepository;
import com.burak.cafe.dao.CategoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryDAOImp implements CategoryDAO {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> findAll() {
        return (List<Category>) categoryRepository.findAll();
    }

    @Override
    public Category findOne(long categoryID) {
        return null;
    }
}
