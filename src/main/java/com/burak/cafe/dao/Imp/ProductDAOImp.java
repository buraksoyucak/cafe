package com.burak.cafe.dao.Imp;

import com.burak.cafe.entity.Product;
import com.burak.cafe.repositories.ProductRepository;
import com.burak.cafe.dao.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
public class ProductDAOImp implements ProductDAO {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product findOne(long userid) {
        return null;
    }
}
