package com.burak.cafe.dao;

import com.burak.cafe.entity.Category;

import java.util.List;

public interface CategoryDAO {

    Category save(Category category);
    List<Category> findAll();
    Category findOne(long categoryID);

}
