package com.burak.cafe.dao;

import com.burak.cafe.entity.Product;

import java.util.List;

public interface ProductDAO {

    Product save(Product suer);
    List<Product> findAll();
    Product findOne(long userid);

}
